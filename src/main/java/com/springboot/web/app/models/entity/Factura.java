package com.springboot.web.app.models.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

@Entity
@Table(name = "factura") // tablas en plural
public class Factura implements Serializable {// buena practica clases CAMEL CASE

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String descripcion;
	private String observacion;
	@Temporal(TemporalType.DATE)
	@Column(name = "create_at") // campos separados por _
	private Date createAt;
	@ManyToOne(fetch = FetchType.LAZY) // muchas facturas un cliente
	private Cliente cliente;
	
	@OneToMany(fetch=FetchType.LAZY, cascade=CascadeType.ALL)//relacion a item
	@JoinColumn(name="factura_id")//llave forranea unidireccional
	private List<ItemFactura> items;
	

	public Factura() {
		this.items = new ArrayList<ItemFactura>();
	}

	@PrePersist
	public void prePersist() {
		createAt = new Date();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getObservacion() {
		return observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}

	public Date getCreateAt() {
		return createAt;
	}

	public void setCreateAt(Date createAt) {
		this.createAt = createAt;
	}

	public Cliente getCliente() {
		return cliente;
	}
	

	public List<ItemFactura> getItems() {
		return items;
	}
	
	public void addItemFacutra(ItemFactura item) {
		this.items.add(item);
	}

	public void setItems(List<ItemFactura> items) {
		this.items = items;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	
	public Double getTotal() {
		Double total = 0.0;
		int size = items.size();
		
		for(int i=0; i<size; i++) {
			total += items.get(i).calcularImporte();
		}
		return total;
	}

	private static final long serialVersionUID = 1L;
}
