package com.springboot.web.app.models.dao;

import org.springframework.data.repository.CrudRepository;

import com.springboot.web.app.models.entity.*;


public interface IClientesDao extends CrudRepository<Cliente, Long> {
	
}
